package SignIn;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import java.util.concurrent.TimeUnit;
import java.time.Duration;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.support.ui.ExpectedConditions;
import java.io.IOException;
public class GmailAcctCreation {
public static void main(String[] args){
WebDriver driver=new ChromeDriver();
System.setProperty("webdriver.chrome.driver", "C:/Users/Admin/Downloads/chromedriver_win32");
driver.manage().window().maximize();
driver.get("https://mail.google.com/mail/u/0/#inbox");
//Enter mail Id
driver.findElement(By.xpath("//input[@class='whsOnd zHQkBf']")).sendKeys("cogproject870@gmail.com");
driver.findElement(By.xpath("//button[@class='VfPpkd-LgbsSe VfPpkd-LgbsSe-OWXEXe-k8QpJ VfPpkd-LgbsSe-OWXEXe-dgl2Hf nCP5yc AjY5Oe DuMIQc LQeN7 qIypjc TrZEUc lw1w4b']")).click();
driver.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS);

//Enter Password
WebDriverWait w=new WebDriverWait(driver, Duration.ofSeconds(40));
w.until(ExpectedConditions.visibilityOfElementLocated (By.name("Passwd")));
driver.findElement(By.name("Passwd")).sendKeys("QWEqwe123");
driver.findElement(By.id("passwordNext")).click();

//Compose mail
w.until(ExpectedConditions.visibilityOfElementLocated (By.xpath("//div[@class='T-I T-I-KE L3']")));
driver.findElement(By.xpath("//div[@class='T-I T-I-KE L3']")).click();
//send mail
w.until(ExpectedConditions.visibilityOfElementLocated (By.xpath("//input[@class='agP aFw']")));
driver.findElement(By.xpath("//input[@class='agP aFw']")).sendKeys("cogproject870@gmail.com");
driver.findElement(By.xpath("//div[@class='Am Al editable LW-avf tS-tW']")).click();
driver.findElement(By.xpath("//div[@class='Am Al editable LW-avf tS-tW']")).sendKeys("Hi!Have a nice day.");
driver.findElement(By.xpath("//div[@class='T-I J-J5-Ji aoO v7 T-I-atl L3']")).click();
driver.manage().timeouts().implicitlyWait(40,TimeUnit.SECONDS);

//create a new label
driver.findElement(By.xpath("//div[@class='aAu arN']")).click();

//Assign a name
driver.findElement(By.xpath("//input[@class='xx']")).sendKeys("CogProject");
driver.findElement(By.xpath("//button[@class='J-at1-auR']")).click();
driver.manage().timeouts().implicitlyWait(40,TimeUnit.SECONDS);
//label the mail
//click on the sent mail

w.until(ExpectedConditions.visibilityOfElementLocated (By.xpath("//div[@class='yW']")));
driver.findElement(By.xpath("//div[@class='yW']")).click();
driver.manage().timeouts().implicitlyWait(60,TimeUnit.SECONDS);

//click on the label
w.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\":b3\"]")));
driver.findElement(By.xpath("//*[@id=\":b3\"]")).click();


//click on the project name
w.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@class='J-LC-Jz']")));
driver.findElement(By.xpath("//div[@class='J-LC-Jz']")).click();

driver.findElement(By.xpath("//div[@class='J-LC J-Ks-KO J-LC-JR-Jp']"));
//driver.quit();
}
}